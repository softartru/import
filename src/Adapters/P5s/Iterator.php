<?php

namespace Siteset\Import\Adapters\P5s;

use Exception;
use Log;

class Iterator implements \Iterator
{
	/**
	 *
	 *
	 */

	private $filename;

	/**
	 *
	 *
	 */

	private $handle;

	/**
	 *
	 *
	 */

	private $buffer;

	/**
	 *
	 *
	 */

	private $key;

	/**
	 *
	 *
	 */

	private $assort;

	/**
	 *
	 *
	 */

	private $dataset;

	/**
	 *
	 *
	 */

	public function __construct($filename)
	{
		$this->filename = $filename;
	}

	/**
	 *
	 *
	 */

	public function rewind()
	{
		$this->handle	= fopen($this->filename, 'r');
		$this->key		= 0;
		$this->assort	= 0;
	}

	/**
	 *
	 *
	 */

	public function current()
	{
		return new Row($this->dataset, $this->assort - 1);
	}

	/**
	 *
	 *
	 */

	public function key()
	{
		return $this->key;
	}

	/**
	 *
	 *
	 */

	public function next()
	{
		--$this->assort;
		++$this->key;
	}

	/**
	 *
	 *
	 */

	public function valid()
	{
		// если есть еще ассортимент
		if ($this->assort > 0)
			return true;

		// сбрасываем данные
		$this->dataset = null;

		// считываем файл побайтово
		while (($buffer = fgets($this->handle, 4096)) !== false) {
			// заполняем буфер
			$this->buffer .= $buffer;

			// если есть 2 тега product
			if (preg_match('/(<product.*?<\/product>)/s', $this->buffer, $matches)) {
				// чистим буфер
				$this->buffer	= preg_replace('/<product.*?<\/product>/s', '', $this->buffer);
				// сохраняем данные
				$this->dataset	= $matches[0];

				try {
					$this->assort	= call_user_func(function () {
						$xml = simplexml_load_string($this->dataset);

						if (!$xml->assortiment->count())
							return 0;

						return $assortiment = $xml->assortiment->children()->count();
					});

					// говорим что итерация валидна
					return true;
				}
				catch (Exception $e) {
					Log::error($e->getMessage());
					Log::error($this->dataset);

					$this->dataset	= null;
					$this->assort	= 0;
				}
			}
		}

		// закрываем поток
		fclose($this->handle);

		// итерация не валидна
		return false;
	}
}
