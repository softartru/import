<?php

namespace Siteset\Import;

use Carbon\Carbon;

class File
{
	/**
	 *
	 *
	 */

	private $property = [];

	/**
	 *
	 *
	 */

	public function __construct($file)
	{
		$this->property = [
			'name'			=> pathinfo($file, PATHINFO_FILENAME),
			'basename'		=> basename($file),
			'dirname'		=> pathinfo($file, PATHINFO_DIRNAME),
			'extension'		=> pathinfo($file, PATHINFO_EXTENSION),
			'updated_at'	=> Carbon::parse(date('Y-m-d H:i:s', filemtime($file))),
			'size'			=> filesize($file),
			'mimetype'		=> mime_content_type($file),
		];
	}

	/**
	 *
	 *
	 */

	public function __get($name)
	{
		return $this->property[$name] ?? null;
	}
}
