<?php

namespace Siteset\Import;

use Illuminate\Support\Str;

class Import
{
	/**
	 *
	 *
	 */

	public function chunk($records, $quantity)
	{
		return new Chunk($records, $quantity);
	}

	/**
	 *
	 *
	 */

	public function get($filename, $adapter, $chunk = false)
	{
		$className = "\Siteset\Import\Adapters\\" . Str::studly(str_replace('-', '_', $adapter)) . "\Iterator";

		return new $className($filename);
	}

	/**
	 *
	 *
	 */

	public function loading($filename, $adapter)
	{
		$loader = "\Siteset\Import\Adapters\\" . Str::studly(str_replace('-', '_', $adapter)) . "\Loader";

		return $loader::load($filename);
	}
}
