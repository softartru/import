<?php

namespace Siteset\Import;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
	/**
	 *
	 * @return void
	 */

	public function register() //: void
	{
		$this->publishes([
			__DIR__ . '/../config/import.php' => config_path('import.php')
		], 'config');
	}

	/**
	 *
	 * @return void
	 */

	public function boot() //: void
	{
		//
	}
}
