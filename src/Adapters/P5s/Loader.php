<?php

namespace Siteset\Import\Adapters\P5s;

use Siteset\Import\File;

class Loader
{
	/**
	 *
	 *
	 */

	public static function load($filename)
	{
		// открываем поток
		$handle = fopen(config('import.sources.p5s'), 'r');

		file_put_contents($filename, $handle);

		// закрываем поток
		fclose($handle);

		return new File($filename);
	}
}
