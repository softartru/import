<?php

namespace Siteset\Import;

class Chunk implements \Iterator
{
	/**
	 *
	 *
	 */

	private $key;

	/**
	 *
	 *
	 */

	private $quantity;

	/**
	 *
	 *
	 */

	private $dataset;

	/**
	 *
	 *
	 */

	private $records;

	/**
	 *
	 *
	 */

	private $finished;

	/**
	 *
	 *
	 */

	public function __construct($records, $quantity)
	{
		$this->records	= $records;
		$this->quantity	= $quantity;
	}

	/**
	 *
	 *
	 */

	public function rewind()
	{
		$this->records->rewind();
		$this->key = 0;
		$this->finished	= false;
	}

	/**
	 *
	 *
	 */

	public function current()
	{
		return $this->dataset->toArray();
	}

	/**
	 *
	 *
	 */

	public function key()
	{
		return $this->key;
	}

	/**
	 *
	 *
	 */

	public function next()
	{
		++$this->key;
	}

	/**
	 *
	 *
	 */

	public function valid()
	{
		if (true === $this->finished)
			return false;

		$this->dataset = collect();

		for ($i = 0; $i < $this->quantity; $i++) {
			if (!$this->records->valid()) {
				$this->finished = true;
				break;
			}

			$this->dataset->push($this->records->current());

			$this->records->next();
		}

		return (boolean)$this->dataset->count();
	}
}
