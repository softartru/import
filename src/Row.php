<?php

namespace Siteset\Import;

abstract class Row
{
	/**
	 *
	 *
	 */

	private $property = [];

	/**
	 *
	 *
	 */

	public function __set($name, $argument)
	{
		// сохраняем данные
		$this->property[$name] = $argument;
	}

	/**
	 *
	 *
	 */

	public function __get($name)
	{
		return $this->property[$name] ?? null;
	}
}
