<?php

namespace Siteset\Import\Adapters\P5s;

class Row extends \Siteset\Import\Row
{
	/**
	 *
	 *
	 */

	public function __construct($data, $key)
	{
		$xml	= simplexml_load_string($data);
		$assort	= call_user_func(function () use ($xml, $key) {
			$assortiment = [];
			foreach ($xml->assortiment->assort as $assort)
				$assortiment[] = $assort;

			return array_reverse($assortiment)[$key];
		});

		$this->name = call_user_func(function () use ($xml, $assort) {
			$name = [trim(strval($xml->attributes()['name']))];

			if ('' !== strval($assort->attributes()['size']))
				$name[] = trim(strval($assort->attributes()['size']));

			if ('' !== strval($assort->attributes()['color']))
				$name[] = trim(strval($assort->attributes()['color']));

			return implode(', ', $name);
		});
		$this->hook				= strval($assort->attributes()['aID']);

		$this->vendor_name		= strval($xml->attributes()['vendor']);
		$this->vendor_code		= strval($xml->attributes()['vendorCode']);

		$this->collection_code	= strval($xml->attributes()['prodID']);
		$this->collection_name	= strval($xml->attributes()['name']);

		$this->leftover			= strval($assort->attributes()['sklad']);
		$this->price			= strval($xml->price->attributes()['BaseRetailPrice']);
		$this->description		= strval($xml->description);

		$this->pictures			= call_user_func(function () use ($xml) {
			$pictures = [];
			foreach ($xml->pictures->picture as $picture)
				$pictures[] = strval($picture);

			return $pictures;
		});

		$this->categories		= call_user_func(function () use ($xml) {
			$categories = [];
			foreach ($xml->categories->category as $category)
				$categories[] = trim(strval($category->attributes()['Name'])) . '/' . trim(strval($category->attributes()['subName']));

			return $categories;
		});

		$this->attributes		= call_user_func(function () use ($xml, $assort) {
			return collect([
				'brutto' 	=> strval($xml->attributes()['brutto']),
				'batteries'	=> strval($xml->attributes()['batteries']),
				'pack'		=> strval($xml->attributes()['pack']),
				'material'	=> strval($xml->attributes()['material']),
				'lenght'	=> strval($xml->attributes()['lenght']),
				'diameter'	=> strval($xml->attributes()['diameter']),
				'size'		=> strval($assort->attributes()['size']),
				'color'		=> strval($assort->attributes()['color']),
			])->filter(function ($value) {
				return !empty($value);
			})->toArray();
		});
	}
}
