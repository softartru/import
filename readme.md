# Импорт

## Установка

В **/config/app.php** добавить

```php
'providers' => [
    ...,
    Siteset\Import\ServiceProvider::class,
    ...,
],

'aliases' => [
    ...,
    'Import' => Siteset\Import\Facade::class,
    ...,
],
```

Вызвать

```php
composer dump-autoload
```

для обновление автозагрузчика.

Вызвать

```
php artisan vendor:publish --provider="Siteset\Import\ServiceProvider"
```

для копирование конфигурационных файлов.

## Использование

Вызывать вот так:

```
$rows = Import::get('путь к файлу', 'адаптер');
```

возвращает **Iterator** с объектами класса **Siteset\Import\Row**

```
Import::loading('путь для файла', 'адаптер');
```

загружает файл и возвращает объект класса **Siteset\Import\File**

## Список адаптеров

* **p5s** - Поставщик счастья (https://p5s.ru/)