<?php

namespace Siteset\Import;

class Facade extends \Illuminate\Support\Facades\Facade
{
	protected static function getFacadeAccessor()
	{
		return Import::class;
	}
}
